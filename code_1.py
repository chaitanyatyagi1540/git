import pandas as pd
from datetime import timedelta

def find_consecutive_days(data):
    # Convert Time column to datetime objects
    data['Time'] = pd.to_datetime(data['Time'])

    # Sort the data by Employee Name and Time
    data = data.sort_values(by=['Employee Name', 'Time'])

    consecutive_days = []
    current_streak = 1
    previous_employee = None
    previous_date = None

    # Iterate through each row in the DataFrame
    for index, row in data.iterrows():
        if row['Employee Name'] != previous_employee or previous_date is None:
            # Start a new streak if it's a different employee or the first row
            current_streak = 1
            # Current date is within same day
        elif row['Time'].date() == previous_date:
            continue
        else:
            # Check if the current date is consecutive to the previous date or within the same day
            if (row['Time'].date() - previous_date).days < 2:
                current_streak += 1
            else:
                current_streak = 1

        # Update previous employee and date for the next iteration
        previous_employee = row['Employee Name']
        previous_date = row['Time'].date()

        # If the streak reaches 7 days, add the employee to the list
        if current_streak == 7:
            consecutive_days.append((row['Employee Name'], row['Position ID']))

    return consecutive_days

def more_than_14_hours_single_shift(data):
    # Convert Time and Time Out columns to datetime objects
    data['Time'] = pd.to_datetime(data['Time'])
    data['Time Out'] = pd.to_datetime(data['Time Out'])

    # Calculate shift duration
    data['Shift Duration'] = data['Time Out'] - data['Time']

    # Filter employees who worked more than 14 hours in a single shift
    filtered_data = data[data['Shift Duration'] > timedelta(hours=14)]
    employees_more_than_14_hours = filtered_data[['Employee Name', 'Position ID']].drop_duplicates()
    return employees_more_than_14_hours.values.tolist()


def less_than_10_hours_between_shifts(data):
    # Convert 'Time' and 'Time Out' columns to datetime
    data['Time'] = pd.to_datetime(data['Time'])
    data['Time Out'] = pd.to_datetime(data['Time Out'])

    # Group by 'Employee Name' and date, then shift 'Time Out' to get the start time of the next shift
    data['Next Shift Start'] = data.groupby(['Employee Name', data['Time'].dt.date])['Time Out'].shift(-1)

    # Calculate the break between shifts as the difference between 'Next Shift Start' and 'Time Out'
    data['Shift Break'] = data['Next Shift Start'] - data['Time Out']

    # Filter out rows where the gap between shifts is less than 10 hours
    filtered_data = data[data['Shift Break'] >= timedelta(hours=10)]

    if filtered_data.empty:
        # If no shifts meet the criteria, return an empty list
        return []
    else:
        # Drop duplicates based on 'Employee Name' to keep only the first occurrence
        unique_employees = filtered_data.drop_duplicates(subset='Employee Name')
        # Return a list of unique employee names
        return unique_employees[['Employee Name', 'Position ID']].values.tolist()

# Example usage:
# Replace 'data' with your actual DataFrame containing the shift data
# result = less_than_10_hours_between_shifts(data)




# Load Excel data
excel_data = pd.read_excel("input.xlsx", engine='openpyxl')

# Task a: Find employees who worked for 7 consecutive days
consecutive_days_result = find_consecutive_days(excel_data)

# Task b: Find employees with less than 10 hours between shifts but greater than 1 hour
less_than_10_hours_result = less_than_10_hours_between_shifts(excel_data)

# Task c: Find employees who worked for more than 14 hours in a single shift
more_than_14_hours_result = more_than_14_hours_single_shift(excel_data)

# Determine the maximum length of the lists
max_length = max(len(consecutive_days_result), len(less_than_10_hours_result), len(more_than_14_hours_result))

# Ensure each list has at least one element, or append None if the list is empty
consecutive_days_result += [None] * (max_length - len(consecutive_days_result))
more_than_14_hours_result += [None] * (max_length - len(more_than_14_hours_result))
less_than_10_hours_result += [None] * (max_length - len(less_than_10_hours_result))

# Create DataFrame with equal-length lists
tasks_results = pd.DataFrame({
    'Employees with 7 consecutive days (Task a)': [item[0] if item else None for item in consecutive_days_result],
    'Position ID (Task a)': [item[1] if item else None for item in consecutive_days_result],
    'Employees with less than 10 hours between shifts (Task b)': [item[0] if item else None for item in less_than_10_hours_result],
    'Position ID (Task b)': [item[1] if item else None for item in less_than_10_hours_result],
    'Employees with more than 14 hours in a single shift (Task c)': [item[0] if item else None for item in more_than_14_hours_result],
    'Position ID (Task c)': [item[1] if item else None for item in more_than_14_hours_result]
})

# Save results to output.xlsx
tasks_results.to_excel("output.xlsx", index=False)
